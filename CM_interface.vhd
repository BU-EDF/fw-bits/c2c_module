library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.types.all;
use work.uC_LINK.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity CM_intf is
  generic (
    LINK_COUNT       : integer);            
  port (
    clk             : in std_logic;               -- 50MHz oscillator input
    reset           : in std_logic;               -- reset to picoblaze

    reprogram_addr  : in  std_logic_vector(10 downto 0);
    reprogram_wen   : in  std_logic;
    reprogram_ren   : in  std_logic;
    reprogram_dv    : out std_logic;                    
    reprogram_di    : in  std_logic_vector(17 downto 0);
    reprogram_do    : out std_logic_vector(17 downto 0);
    reprogram_reset : in  std_logic;               -- reset the picoblaze for reprogramming
    
    -- UART_INT
    UART_Rx         : in  std_logic;              -- serial in
    UART_Tx         : out std_logic := '1';       -- serial out

    --monitoring
    irq_count       :  in  std_logic_vector(31 downto 0);
    link_INFO_in    :  in  C2CLinkInfo_in_array_t (0 to LINK_COUNT-1);
    link_INFO_out   :  out C2CLinkInfo_OUT_array_t(0 to LINK_COUNT-1)

    );
end entity CM_intf;

architecture behavioral of CM_intf is
  signal reset: std_logic;
begin
  --reset
  reset <= not reset_axi_n;


  -------------------------------------------------------------------------------
  -- AXI 
  -------------------------------------------------------------------------------
  reprogram_dv <= reprogram_ren;
  uC_1: entity work.uC
    generic map(
      LINK_COUNT => LINK_COUNT)
    port map (
      clk                   => clk_axi,
      reset                 => reset,
      reprogram_addr        => reprogram_addr
      reprogram_wen         => reprogram_wen,
      reprogram_di          => reprogram_di,
      reprogram_do          => reprogram_do,
      reprogram_reset       => reprogram_reset,
      UART_Rx               => UART_Rx,
      UART_Tx               => UART_Tx,
      irq_count             => irq_count,
      link_INFO_in          => uC_link_INFO_in (0 to LINK_COUNT-1),
      link_INFO_out         => uC_link_INFO_out(0 to LINK_COUNT-1)
      );
  
end architecture behavioral;
